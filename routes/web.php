<?php

use App\Http\Controllers\HistoryTransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::prefix('transaction')->name('transaction.')->group(function() {
        Route::get('/', function() {
            return view('transactions.index');
        })->name('index');
    });

    Route::prefix('history')->name('history.')->group(function() {
        Route::get('/', function() {
            return view('history.index');
        })->name('index');

        Route::get('history-list', [HistoryTransactionController::class, 'makeTable'])->name('list');
    });

});
