<x-form-section submit="topUpBalance">
    <x-slot name="title">
        {{ __('Top Up Balance') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Top up your balance from multiple sources.') }}
    </x-slot>

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-label for="method" value="{{ __('Choose Method') }}" />
            <select wire:model="state.method" id="method" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm w-full">
                <option selected="selected" disabled="disabled"></option>
                <option value="minimarket">Minimarket</option>
                <option value="debit-visa">Debit VIsa/Mastercard</option>
                <option value="atm">ATM</option>
                <option value="virtual-account">Virtual Account</option>
            </select>
            <x-input-error for="method" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="amount" value="{{ __('Nominal Topup') }}" />
            <x-input id="amount" type="number" class="mt-1 block w-full" wire:model="state.amount"/>
            <x-input-error for="amount" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="notes" value="{{ __('Notes') }}" />
            <x-input id="notes" type="text" class="mt-1 block w-full" wire:model="state.notes"/>
            <x-input-error for="notes" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="evidence" value="{{ __('Upload Evidence') }}" />
            <x-input id="evidence" type="file" class="mt-1 block w-full" wire:model="state.evidence_file" accept="image/*"/>
            <div wire:loading wire:target="evidence_file">Uploading...</div>
            <x-input-error for="evidence" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-action-message class="me-3" on="saved">
            {{ __('Saved.') }}
        </x-action-message>

        <x-button>
            {{ __('Continue') }} <i class="lni lni-arrow-right"></i>
        </x-button>
    </x-slot>
</x-form-section>
