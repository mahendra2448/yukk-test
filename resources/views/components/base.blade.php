<div class="p-6 lg:p-8 bg-white dark:bg-gray-800 dark:bg-gradient-to-bl dark:from-gray-700/50 dark:via-transparent border-b border-gray-200 dark:border-gray-700">

    <h1 class="text-2xl font-medium text-gray-900 dark:text-white">
        Welcome back, {{ auth()->user()->name }}!
    </h1>

    <p class="mt-2 text-gray-500 dark:text-gray-300 leading-relaxed">
        Your balance: <strong>Rp {{ number_format(getCurrentBalance(), 0, '.', ',')}}</strong>
    </p>
</div>

<div class="bg-gray-200 dark:bg-gray-800 bg-opacity-25 grid grid-cols-1 md:grid-cols-2 gap-6 lg:gap-8 p-6 lg:p-8">
    <a href="{{ route('transaction.index') }}" wire:navigate class="block h-full rounded-lg border border-gray-700 p-6 hover:border-cyan-300">
        <div class="flex items-center">
            <i class="lni lni-control-panel text-gray-900 dark:text-white"></i>
            <h2 class="ms-3 text-xl font-semibold text-gray-900 dark:text-white">
                Transaction
            </h2>
        </div>

        <p class="mt-4 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
            Top up your balance or transfer to multiple banks or wallet.
        </p>
    </a>

    <a href="{{ route('history.index') }}" class="block h-full rounded-lg border border-gray-700 p-6 hover:border-yellow-400">
        <div class="flex items-center">
            <i class="lni lni-timer text-gray-900 dark:text-white"></i>
            <h2 class="ms-3 text-xl font-semibold text-gray-900 dark:text-white">
                History
            </h2>
        </div>

        <p class="mt-4 text-gray-500 dark:text-gray-400 text-sm leading-relaxed">
            Track your income and expenses more details.
        </p>
    </a>
</div>


