@push('page_css')
@include('layouts.datatables_css')
@endpush

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-yellow-400 leading-tight">
            {{ __('History') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8 text-gray-800 dark:text-gray-200 leading-tight">

            <table id="the-table" class="table table-auto text-md">
                <thead></thead>
                {{-- loaded by server-side datatables --}}
            </table>
        </div>
    </div>

@push('page_scripts')
@include('layouts.datatables_js')
<script>
    console.log(window.location.href + '/history-list');

    $('#the-table thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#the-table thead');

    $('#the-table').DataTable({
        order: [[ 0, 'desc' ]],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel" style="color: green;"></i> Excel',
                action: exportExcelFullRows, //it will export full rows
                title: 'Transaction History'
            },
            {
                extend: 'pdf',
                text: '<i class="fas fa-file-pdf" style="color: red;"></i> PDF',
                title: 'Transaction History'
            }
        ],
        processing: true,
        serverSide: true,
        delaySearch: 500,
        ajax: window.location.href + '/history-list',
        columns: [
            { data: 'created_at', name: 'created_at', title: 'Created At'},
            { data: 'trx_code', name: 'trx_code', title: 'Transaction Code'},
            { data: 'type', name: 'type', title: 'Transaction Type', className: 'text-center'},
            { data: 'amount', name: 'amount', title: 'Amount',
                render: function(data, type, row, meta) {
                    return '<div class="text-right"><span class="float-left">Rp</span>' + new Intl.NumberFormat().format(data) +
                        '</div>'
                }
            },
            { data: 'description', name: 'description', title: 'Description'},
        ],
        orderCellsTop: true,
        initComplete: function () {
            var api = this.api();
 
            // For each column
            api
                .columns()
                .eq(0)
                .each(function (colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );

                    var title = $(cell).text();
                    $(cell).html('<input class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm" type="text" placeholder="Filter ' + title + '" />');

                    if (title == 'Actions') {
                        $(cell).html('')
                    }
 
                    // On every keypress in this input
                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                        .off('keyup change')
                        .on('change', function (e) {
                            // Get the search value
                            $(this).attr('title', $(this).val());
                            var regexr = '({search})'; //$(this).parents('th').find('select').val();
 
                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api
                                .column(colIdx)
                                .search(
                                    this.value != ''
                                        ? regexr.replace('{search}', '(((' + this.value + ')))')
                                        : '',
                                    this.value != '',
                                    this.value == ''
                                )
                                .draw();
                        })
                        .on('keyup', function (e) {
                            e.stopPropagation();
 
                            $(this).trigger('change');
                            $(this)
                                .focus()[0]
                                .setSelectionRange(cursorPosition, cursorPosition);
                        });
                });
        },
    });
</script>
@endpush

</x-app-layout>