<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
    use HasFactory;

    protected $table = 'app_transactions';
    protected $fillable = [
        'id',
        'user_id',
        'type',
        'trx_code',
        'amount',
        'notes',
        'status',
        'evidence_file',
        'source_method',
        'source_name',
        'destination_bank',
        'destination_name',
        'destination_code',
        'additional_fee',
        'trx_date',
        'trx_signature',
    ];
    
    public static function rules()
    {
        return [
            'user_id'       => 'required',
            'type'          => 'required',
            'trx_code'      => 'required',
            'amount'        => 'required',
            'trx_date'      => 'required',
            'status'        => 'required',
            'source_method' => 'required',
            'evidence_file' => 'required',
        ];
    }
}
