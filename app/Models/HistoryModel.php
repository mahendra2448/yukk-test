<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryModel extends Model
{
    use HasFactory;

    protected $table = 'app_history_transaction';
    protected $fillable = [
        'id',
        'transaction_id',
        'user_id',
        'type',
        'description',
        'amount',
        'history_key',
    ];

    public static function rules() {
        return [
            'transaction_id'    => 'required',
            'user_id'           => 'required',
            'type'              => 'required',
            'amount'            => 'required',
            'history_key'       => 'required',
        ];
    }


    /**
     * Modify created_at when accessed.
     */
    protected function getCreatedAtAttribute($value)
    {
        return date('d/M/Y H:i:s', strtotime($value));
    }
}
