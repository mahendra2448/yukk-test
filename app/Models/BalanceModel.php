<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceModel extends Model
{
    use HasFactory;

    protected $table = 'app_balance';
    protected $fillable = [
        'id',
        'user_id',
        'balance',
        'last_topup_date',
        'last_topup_amount',
        'source'
    ];

    public static function rules() {
        return [
            'user_id'           => 'required',
            'balance'           => 'required',
            'source'            => 'required',
        ];
    }
}
