<?php

namespace App\Constants;

final class GlobalConstant {
    
    public const TRUE = true;
    public const FALSE = false;

    public const ZERO_AMOUNT = 0;

    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';

    public const TYPE_TOPUP = 'topup';
    public const TYPE_TRANSFER = 'transfer';
    
    public const METHOD_MINIMARKET = 'minimarket';
    public const METHOD_WITH_FEE = [
        'minimarket',
        'wallet',
        'bank'
    ];

    public const IMAGE_STORAGE_PATH = 'app/public/transaction-evidences/';
    public const IMAGE_PATH = 'storage/transaction-evidences/';

    public const HISTORY_DESCRIPTION = 'Admin Fee';
} 