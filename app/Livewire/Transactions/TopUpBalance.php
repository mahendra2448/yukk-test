<?php

namespace App\Livewire\Transactions;

use App\Constants\GlobalConstant as Cons;
use App\Traits\Transactions\SaveTransactionTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Livewire\Features\SupportFileUploads\WithFileUploads;

class TopUpBalance extends Component
{
    use WithFileUploads, SaveTransactionTrait;
    
    /**
     * The component's state.
     *
     * @var array
     */
    public $state = [
        'type' => Cons::TYPE_TOPUP,
        'method' => '',
        'amount' => '',
        'notes' => '',
        'evidence_file' => '',
    ];

    public function topUpBalance()
    {
        $this->resetErrorBag();

        try {
            DB::beginTransaction();

            $processTrx = $this->processTransaction($this->state);            
            if (empty($processTrx)) {
                throw new \Exception("Error Processing Transaction", 1);                
            }

            $processHistory = $this->processHistory($processTrx, false);
            if (empty($processHistory)) {
                throw new \Exception("Error Processing History Transaction", 1);
            }

            $processBalance = $this->processBalance($processTrx);
            if (empty($processBalance)) {
                throw new \Exception("Error Processing Balance", 1);                
            }
            // dd(48, $processTrx, $processHistory, $processBalance);
            
            DB::commit();
            
            if ($processTrx->additional_fee > 0 && !empty($processBalance)) {
                $this->processHistory($processTrx, true);
            }

            $this->state = [
                'type' => Cons::TYPE_TOPUP,
                'method' => '',
                'amount' => '',
                'notes' => '',
                'evidence_file' => '',
            ];
    
            Log::info("Transaction saved successfully. TrxCode: " . $processTrx->trx_code);
            $this->dispatch('saved');
        } catch (\Throwable $th) {
            DB::rollBack();
            $this->state = [
                'type' => Cons::TYPE_TOPUP,
                'method' => '',
                'amount' => '',
                'notes' => '',
                'evidence_file' => '',
            ];

            Log::error("Errs: " . $th->getMessage() . ' line: ' . $th->getLine());
            $this->dispatch('failed');
        }

    }

    public function render()
    {
        return view('livewire.transactions.top-up-balance');
    }
}
