<?php

namespace App\Traits\Transactions;

use App\Constants\GlobalConstant as Cons;
use App\Models\BalanceModel;
use App\Models\HistoryModel;
use App\Models\TransactionModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use Illuminate\Support\Str;
use Intervention\Image\Drivers\Gd\Driver;

trait SaveTransactionTrait
{

    public function processTransaction($state) {

        // dd($state['evidence_file']->getClientOriginalName(), $state['evidence_file']);
        $user = auth()->user();
        $file = $state['evidence_file'];

        // Arrange fields
        $userID         = $user->id;
        $type           = $state['type'];
        $trxCode        = Str::uuid();
        $amount         = $state['amount'];
        $notes          = $state['notes'];
        $status         = Cons::STATUS_SUCCESS;
        $sourceMethod   = $state['method'];
        $additionalFee  = getAdditionalFee($sourceMethod);
        $trxDate        = now();
        $trxSignKey     = hash('sha256', $userID . $trxCode . $status);

        // Image
        $imageName  = date('[Y-m-d]-') . $trxSignKey . '-' . strtoupper($status) . '.' . $file->extension();
        $imageSave  = storage_path(Cons::IMAGE_STORAGE_PATH . $imageName);
        $imagePath  = Cons::IMAGE_PATH . $imageName;        
        $manager    = new ImageManager(Driver::class);
        $image      = $manager->read($file);
        $image->save($imageSave, 50);

        $row = [
            'user_id'           => $userID,
            'type'              => $type,
            'trx_code'          => $trxCode,
            'amount'            => $amount,
            'notes'             => $notes,
            'status'            => $status,
            'evidence_file'     => $imagePath,
            'source_method'     => $sourceMethod,
            'source_name'       => optional($state)['source_name'],
            'destination_bank'  => optional($state)['destination_bank'],
            'destination_name'  => optional($state)['destination_name'],
            'destination_code'  => optional($state)['destination_code'],
            'additional_fee'    => $additionalFee,
            'trx_date'          => $trxDate,
            'trx_signature'     => $trxSignKey,
        ];

        $validated = Validator::make($row, TransactionModel::rules());

        if ($validated->fails()) {
            $msg = $validated->errors()->first();
            Log::error('Error saving Transaction: ' . $msg);
            return false;
        
        } else {
            return TransactionModel::create($row);
        }
    }
    
    /**
     * Processing transaction history
     */
    public function processHistory($trx, $addFee) {
        
        $description = $trx->notes;
        $amount = $trx->amount;

        if ($addFee) { 
           $description = Cons::HISTORY_DESCRIPTION;
           $amount = $trx->additional_fee;
        }

        $rowHistory = [
            'transaction_id'    => $trx->trx_code,
            'user_id'           => $trx->user_id,
            'type'              => $trx->type,
            'description'       => $description,
            'amount'            => $amount,
            'history_key'       => hash('sha256', $trx->id . $trx->type . now()),
        ];
        
        $validated = Validator::make($rowHistory, HistoryModel::rules());

        if ($validated->fails()) {
            $msg = $validated->errors()->first();
            Log::error('Error saving History: ' . $msg);
            return false;
        
        } else {
            return HistoryModel::create($rowHistory);
        }
    }


    /**
     * Processing user balance
     */
    public function processBalance($trx) {

        $balance  = $trx->amount;
        $lastTopupDate = null;
        $lastTopupAmount = null;
        $existing = BalanceModel::where('user_id', $trx->user_id)->latest()->first();

        if (!empty($existing)) {
            $lastTopupDate      = $existing->created_at;
            $lastTopupAmount    = $existing->last_topup_amount;

            $balance = ($trx->type == Cons::TYPE_TOPUP) 
                        ? $existing->balance + $balance     // for topup
                        : $existing->balance - $balance;    // for transfer

            $balance = ($trx->additional_fee > 0) 
                        ? $balance - $trx->additional_fee 
                        : $balance;
        }

        $rowBalance = [
            'user_id'           => $trx->user_id,
            'balance'           => $balance,
            'last_topup_date'   => $lastTopupDate,
            'last_topup_amount' => $lastTopupAmount,
            'source'            => $trx->source_method,
        ];
        
        $validated = Validator::make($rowBalance, BalanceModel::rules());

        if ($validated->fails()) {
            $msg = $validated->errors()->first();
            Log::error('Error saving Balance: ' . $msg);
            return false;
        
        } else {
            return BalanceModel::create($rowBalance);
        }
    }
}