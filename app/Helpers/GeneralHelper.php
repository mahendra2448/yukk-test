<?php

use App\Constants\GlobalConstant;
use App\Models\BalanceModel;

if (! function_exists('getAdditionalFee')) {
    /**
     * Helper to determine additional fee to transaction
     *
     * @return int
     */
    function getAdditionalFee($method)
    {
        $fee = GlobalConstant::ZERO_AMOUNT;

        if (in_array($method, GlobalConstant::METHOD_WITH_FEE)) {
            $fee = ($method == GlobalConstant::METHOD_MINIMARKET) ? 1500 : 1000;
        }

        return $fee;
    }
}

if (! function_exists('getCurrentBalance')) {
    /**
     * Helper to determine additional fee to transaction
     *
     * @return int
     */
    function getCurrentBalance()
    {
        $userID = auth()->user()->id;
        $balance = BalanceModel::select('user_id', 'balance')
                    ->where('user_id', $userID)
                    ->latest()
                    ->first()
                    ->balance;
        
        return $balance;
    }
}
