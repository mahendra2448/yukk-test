<?php

namespace App\Http\Controllers;

use App\Models\HistoryModel;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HistoryTransactionController extends Controller
{
    /**
     * Make table on index.blade
     * 
     * @return json
     */
    public function makeTable() {
        $query   = HistoryModel::where('user_id', auth()->user()->id)
                    ->latest()
                    ->get()
                    ->toArray();
                    
        $results = array();
        foreach ($query as $q) {
            $rows   = [
                'id'            => $q['id'],
                'user_id'       => $q['user_id'],
                'type'          => $q['type'],
                'amount'        => $q['amount'],
                'description'   => $q['description'],
                'trx_code'      => $q['transaction_id'],
                'created_at'    => $q['created_at'],
                
            ];
            array_push($results, $rows);
        }

        // dd($results);
        $jsons  = Datatables::of($results)->make(true);
        return $jsons;
    }

}
