<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('app_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('type', 100)->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->uuid('trx_code')->nullable()->default(null);
            $table->bigInteger('amount')->nullable();
            $table->text('notes')->nullable();
            $table->string('status', 100)->nullable();
            $table->string('evidence_file', 255)->nullable();
            $table->string('source_method', 100)->nullable();
            $table->string('source_name', 100)->nullable();
            $table->string('destination_bank', 100)->nullable();
            $table->string('destination_name', 100)->nullable();
            $table->string('destination_code', 100)->nullable();
            $table->integer('additional_fee')->unsigned()->nullable();
            $table->timestamp('trx_date')->nullable();
            $table->string('trx_signature', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaction_models');
    }
};
