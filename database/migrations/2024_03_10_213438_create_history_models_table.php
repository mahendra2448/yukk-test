<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('app_history_transaction', function (Blueprint $table) {
            $table->id();
            $table->uuid('transaction_id')->nullable()->default(null);
            $table->bigInteger('user_id')->nullable();
            $table->string('type', 100)->nullable();
            $table->string('description', 100)->nullable();
            $table->bigInteger('amount')->nullable();
            $table->string('history_key', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('history_models');
    }
};
