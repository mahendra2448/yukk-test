# Fake PaymentApp

## Requirements
- PHP v8.2

## Setup
- Clone or download as zip
- Run `cp .env.example .env`
- Run `touch database/database.sqlite` or create manually
- For simple use, set database to `sqlite` driver as following:
```
DB_CONNECTION=sqlite
# DB_HOST=127.0.0.1
# DB_PORT=3306
# DB_DATABASE=
# DB_USERNAME=
# DB_PASSWORD=
```
- Run `composer update` or `composer install`
- Run `npm install && npm run dev` or `npm run build`
- Run `php artisan key:generate && php artisan migrate`

## Usage
- Run `php artisan serve` and `npm run dev` (if not built yet)
- Open [Register](http://localhost:8000/register) first

## Extension
If you are using Visual Studio Code, then install the following extension:
- [Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
- [SQLite Viewer](https://marketplace.visualstudio.com/items?itemName=qwtel.sqlite-viewer)

