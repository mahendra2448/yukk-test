<?php

namespace Tests\Feature\Livewire\Transactions;

use App\Livewire\Transactions\TopUpBalance;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class TopUpBalanceTest extends TestCase
{
    /** @test */
    public function renders_successfully()
    {
        Livewire::test(TopUpBalance::class)
            ->assertStatus(200);
    }
}
